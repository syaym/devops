package ants.devops.common


/**
 * 代码仓库基础路径
 * @return
 */
static def repositoryUrl(){

    return "http://ants.gitlab.com/root/"
}


/**
 * k8s代码仓库认证
 * @return
 */
static def k8s_gitlab_secret(){
    return "k8s-gitlab"
}

/**
 * 工程项目
 * @return
 */
static def project(){
    return "ants"
}
/**
 * harbor地址
 * @return
 */
static def harborHost(){
    return "192.168.1.123"
}

/**
 * harbor认证证书
 * @return
 */
static def harbor_Secret(){
    return "harbor"
}
