package ants.devops.steps

import ants.devops.common.ProjectProperty

/**
 *
 * @param jobName 工程名称
 * @param dockefilepath dockerfile路径
 * @return
 */
def build(String jobName,String dockefilepath){
    def project = ProjectProperty.project()
    def harborhost = ProjectProperty.harborHost()
    def group = parseJob(jobName)
    def harborauth = ProjectProperty.harbor_Secret()
    container('tools'){
        sh "docker build -t ${harborhost}/${project}/${group}:${env.gitlog} -f ${dockefilepath} ."
        withCredentials([usernamePassword(credentialsId: "${harborauth}", passwordVariable: 'password', usernameVariable: 'username')]) {
            sh "docker login -u ${username} -p ${password} ${harborhost}"
        }
    }
}


def parseJob(String jobname){
    String[] list = jobname.split("^.{3,4}-")
    return list[1]
}