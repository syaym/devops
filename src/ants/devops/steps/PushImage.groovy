package ants.devops.steps

import ants.devops.common.ProjectProperty


def pushImage(String jobName){
    def project = ProjectProperty.project()
    def harborhost = ProjectProperty.harborHost()
    def group = parseJob(jobName)
    container('tools'){
        sh "docker push ${harborhost}/${project}/${group}:${env.gitlog}"
    }
}

def parseJob(String jobname){
    String[] list = jobname.split("^.{3,4}-")
    return list[1]
}