package ants.devops.steps


/**
 *
 * @param jobname job名称
 * @param caseSuite 测试套件名称
 */
def test(String jobname,String caseSuite){
    def testjob = build job: jobname,
               parameters: [
                       string(name:'caseSuite',value:caseSuite)
               ],
               wait: false,
               propagate: false
    def restult = testjob
    print(testjob)
    print(restult)
}


