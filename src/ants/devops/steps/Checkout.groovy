package ants.devops.steps

import ants.devops.common.ProjectProperty


def checkout(String jobname,String branch){
    def job = parseJob(jobname)
    def baseRepositoryUrl = ProjectProperty.repositoryUrl()
    def credentialsId = ProjectProperty.k8s_gitlab_secret()
    container('tools'){
        checkout([$class: 'GitSCM', branches: [[name: "*/${branch}"]], extensions: [], userRemoteConfigs: [[credentialsId: "${credentialsId}", url: "${baseRepositoryUrl}${job}.git"]]])
        def commit =sh(script: "git rev-parse HEAD", returnStdout: true).trim()
        env.gitlog = commit
    }
}


def parseJob(String jobname){
    String[] list = jobname.split("^.{3,4}-")

    return list[1]
}

def parseNameSpace(String jobname){
    String[] list = jobname.split("-")
    return list[0]
}