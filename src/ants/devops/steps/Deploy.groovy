package ants.devops.steps

import ants.devops.common.ProjectProperty


def deploy(String jobName,String deployfilepath){
    def project = ProjectProperty.project()
    def harborhost = ProjectProperty.harborHost()
    def group = parseJob(jobName)
    def namespace = parseNameSpace(jobName)
    container('tools'){
        sh "sed -i s!{{IMAGE}}!${harborhost}/${project}/${group}:${env.gitlog}!g ${deployfilepath}"
        sh "sed -i s/{{NAMESPACE}}/${namespace}/g ${deployfilepath}"
        sh "kubectl apply -f ${deployfilepath}"
    }
}
/**
 * 解析当前job是那个微服务
 * @param jobname
 * @return
 */

def parseJob(String jobname){
    String[] list = jobname.split("^.{3,4}-")
    return list[1]
}
/**
 * 解析当前job部署到那个名称空间，实际根据规则来定义
 * @param jobname
 * @return
 */
def parseNameSpace(String jobname){
    String[] list = jobname.split("-")
    return list[0]
}