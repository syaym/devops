import ants.devops.common.Environment
import ants.devops.steps.Build
import ants.devops.steps.Checkout
import ants.devops.steps.Deploy
import ants.devops.steps.PushImage
import ants.devops.steps.Test

//是的撒多撒
//class Devops {
//    private String jobname
//    private String branch
//    private String dockerfilepath
//    private String casesuit
//    private String deployfilepath
//
//    Devops(String jobname,String branch,String dockerfilepath,String casesuit,String deployfilepath){
//        this.jobname = jobname
//        this.branch = branch
//        this.dockerfilepath  = dockerfilepath
//        this.casesuit = casesuit
//        this.deployfilepath = deployfilepath
//    }
//     void checkout(){
//        new Checkout().checkout(jobname,branch)
//    }
//
//
//    void  build(){
//        new Build().build(jobname,dockerfilepath)
//    }
//
//    void  pushImage(){
//        new PushImage().pushImage(jobname)
//    }
//
//
//    void  test(){
//        new Test().test(testjobname,casesuit)
//    }
//
//    void  deployK8s(){
//        new Deploy().deploy(jobname,deployfilepath)
//    }
//}


def checkout(String jobname,String branch){
    new Checkout().checkout(jobname,branch)
}


def build(String jobname,String dockerfilepath){
    new Build().build(jobname,dockerfilepath)
}

def pushImage(String jobname){
    new PushImage().pushImage(jobname)
}


def test(String testjobname,String casesuit){
    new Test().test(testjobname,casesuit)
}

def deployK8s(String jobname,String deployfilepath){
    new Deploy().deploy(jobname,deployfilepath)
}